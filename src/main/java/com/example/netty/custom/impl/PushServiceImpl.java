package com.example.netty.custom.impl;

import com.example.netty.config.NettyConfig;
import com.example.netty.custom.PushService;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.http.websocketx.TextWebSocketFrame;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

@Service
public class PushServiceImpl implements PushService {


    @Override
    public void pushMsgToOne(String userId, String msg) {
        Channel channel = NettyConfig.getUserChannelMap().get(userId);
        channel.writeAndFlush(msg);
    }

    @Override
    public void pushMsgToAll(String msg) {
        NettyConfig.getChannelGroup().writeAndFlush(new TextWebSocketFrame(msg));
    }
}
